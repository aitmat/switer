<?php

namespace App\Controller;

use App\Entity\Client;
use App\Form\FormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class indexController extends Controller
{
    /**
     * @Route("/", name="index_action")
     * @param Request $request
     * @param \Swift_Mailer $mailer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexPageAction(Request $request, \Swift_Mailer $mailer)
    {
        $error = null;
        $client = new Client();
        $form = $this->createForm(FormType::class, $client);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Client $data */
            $data = $form->getData();
            $message = (new \Swift_Message('Новый клиент'))
                ->setTo('aity_95@mail.ru')
                ->setFrom('aitmat473@gmail.com')
                ->setBody(
                    $this->renderView(
                        'emails/sweater.html.twig', [
                            'name' => $data->getName(),
                            'phone' => $data->getPhone(),
                        ]
                    ),
                    'text/html'
                );
            $mailer->send($message);

            return new Response($this->renderView('success_order.html.twig'));
        } elseif ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('error', 'Пожалуйста, введите корректные данные');
        }
        return $this->render('index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}