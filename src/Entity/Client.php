<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Client
{
    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min=3,
     *     max=20,
     *     minMessage="Пожалуйста, введите корректное имя",
     *     maxMessage="Пожалуйста, введите корректное имя",
     *     )
     */
    private $name;

    /**
     * @Assert\NotBlank()
     * @Assert\Regex(
     *     pattern="/^[0][7,5][0,5,7][0-9]{7}$/",
     *     match=true,
     *     message="Пожалуйста, введите корректный номер телофона"
     * )
     */
    private $phone;

    /**
     * @param mixed $name
     * @return Client
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $phone
     * @return Client
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }


}